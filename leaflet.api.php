<?php

/**
 * @file
 * API documentation for Leaflet.
 */

/**
 * Define one or map definitions to be used when rendering a map.
 *
 * leaflet_map_get_info() will grab every defined map, and the returned
 * associative array is then passed to leaflet_render_map(), along with a
 * collection of features.
 *
 * The settings array maps to the settings available to leaflet map object,
 * http://leaflet.cloudmade.com/reference.html#map-properties
 *
 * Layers are the available base layers for the map and, if you enable the
 * layer control, can be toggled on the map.
 *
 * @return array
 *   Associative array containing a complete leaflet map definition.
 */
function hook_leaflet_map_info() {
  return array(
    'OSM Mapnik' => array(
      'label' => 'OSM Mapnik',
      'description' => t('Leaflet default map.'),
      'settings' => array(
        'dragging' => TRUE,
        'touchZoom' => TRUE,
        'scrollWheelZoom' => TRUE,
        'doubleClickZoom' => TRUE,
        'zoomControl' => TRUE,
        'attributionControl' => TRUE,
        'trackResize' => TRUE,
        'fadeAnimation' => TRUE,
        'zoomAnimation' => TRUE,
        'closePopupOnClick' => TRUE,
        'layerControl' => TRUE,
        // 'minZoom' => 10,
        // 'maxZoom' => 15,
        // 'zoom' => 15, // set the map zoom fixed to 15
      ),
      'layers' => array(
        'earth' => array(
          'urlTemplate' => 'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
          'options' => array(
            'attribution' => 'OSM Mapnik',
            // The switchZoom controls require multiple layers, referencing one
            // another as "switchLayer".
            'switchZoomBelow' => 15,
            'switchLayer' => 'satellite',
          ),
        ),
        'satellite' => array(
          'urlTemplate' => 'http://otile{s}.mqcdn.com/tiles/1.0.0/sat/{z}/{x}/{y}.png',
          'options' => array(
            'attribution' => 'OSM Mapnik',
            'subdomains' => '1234',
            'switchZoomAbove' => 15,
            'switchLayer' => 'earth',
          ),
        ),
      ),
      // Uncomment the lines below to use a custom icon
      // 'icon' => array(
      //   'iconUrl'       => '/sites/default/files/icon.png',
      //   'iconSize'      => array('x' => '20', 'y' => '40'),
      //   'iconAnchor'    => array('x' => '20', 'y' => '40'),
      //   'popupAnchor'   => array('x' => '-8', 'y' => '-32'),
      //   'shadowUrl'     => '/sites/default/files/icon-shadow.png',
      //   'shadowSize'    => array('x' => '25', 'y' => '27'),
      //   'shadowAnchor'  => array('x' => '0', 'y' => '27'),
      // ),
    ),
  );
}

/**
 * Alter $map_info arrays provided by other modules.
 *
 * @param $map_info
 *   Array of leaflet map definition arrays as returned by
 *   hook_leaflet_map_info().
 */
function hook_leaflet_map_info_alter(&$map_info) {
  foreach ($map_info as $map_id => $info) {
    $map_info[$map_id]['settings']['zoom'] = 15; // set the map zoom fixed to 15
  }
}

/**
 * Do additional processing prior to building a map.
 *
 * @param $vars
 *   Associative array of settings to be attached to the map.
 *   - mapId: The unique HTML id of the map.
 *   - map: Map definition as returned my leaflet_map_get_info();
 *   - features: Associative array of map features.
 */
function hook_leaflet_map_prebuild_alter(&$vars) {
  // Example from Leaflet Markercluster module.
  if (!isset($vars['map']['settings']['maxClusterRadius']) || $vars['map']['settings']['maxClusterRadius'] > 0) {
    drupal_add_library('leaflet_markercluster', 'leaflet_markercluster');
    // Increase weight so we're included after 'leaflet.drupal.js'
    $options = array('type' => 'file', 'weight' => 1);
    drupal_add_js(drupal_get_path('module', 'leaflet_markercluster') . '/leaflet_markercluster.drupal.js', $options);
  }
}
