(function ($) {
  Drupal.leaflet_views = {
    bbox: {
      // When a new map is loaded by the leaflet module, init
      // bbox.
      loadMap: function(e, map, lMap) {
        bbox_data_url = map.bbox_data_url || false;
        lMap.leaflet_views = lMap.leaflet_views || {};
        if (bbox_data_url) {
          // If URL is set, boundary box is enabled for map.
          lMap.leaflet_views.bbox_data_url = Drupal.settings.basePath + bbox_data_url;
          lMap.leaflet_views.buffer_ratio = map.buffer_ratio || 0;
          lMap.leaflet_views.lazy_zoom_level = 2;
          lMap.leaflet_views.dom_id = map.view_dom_id || false;

          // Bind event handlers to map.
          lMap.on('moveend', Drupal.leaflet_views.bbox.updateMarkers);
        }
        // Attach behaviors to popup.
        lMap.on('popupopen', Drupal.leaflet_views.bbox.openPopup);
      },

      // Get new features from view, update map.
      updateMarkers: function(e) {
        var lMap = e.target;
        var bounds = lMap.getBounds();

        // Abort previous request if still active.
        // @note this does not cancel the server-side request.
        if (lMap.leaflet_views.current_request) {
          lMap.leaflet_views.current_request.abort();
          lMap.fire('dataload');
        }

        if (lMap.getZoom() < lMap.leaflet_views.lazy_zoom_level) {
          // Skip requests for too low zoom levels.
          return;
        }

        // Determine if a new request is necessary.
        var loaded_bounds = lMap.leaflet_views.loaded_bounds || false;
        if (loaded_bounds && loaded_bounds.contains(bounds)) {
          // Items are already loaded for current bounds.
          return;
        }

        // Extend bounds before fetching data.
        var buffer_ratio = lMap.leaflet_views.buffer_ratio;
        if (buffer_ratio > 0) {
          bounds = bounds.pad(buffer_ratio);
        }

        // @todo add contextual and/or exposed filters to request.
        // Make a new GeoJSON layer.
        var url = lMap.leaflet_views.bbox_data_url + '/' + bounds.toBBoxString();

        // Check if there are any GET parameters to send to views.
        // (queryString parsing stolen from ajax_view.js Drupal.views.ajaxView)
        var queryString = window.location.search || '';
        if (queryString !== '') {
          // Remove the question mark and Drupal path component if any.
          var queryString = queryString.slice(1).replace(/q=[^&]+&?|&?render=[^&]+/, '');
          if (queryString !== '') {
            // If there is a '?' in ajax_path, clean url are on and & should be used to add parameters.
            queryString = ((/\?/.test(url)) ? '&' : '?') + queryString;
          }
        }
        // Fire dataloading event for Leaflet.loading.
        // @see https://github.com/ebrelsford/Leaflet.loading
        lMap.fire('dataloading');
        lMap.leaflet_views.current_request = $.getJSON(url + queryString, function(points) {
          lMap.leaflet_views.current_request = false;
          lMap.leaflet_views.loaded_bounds = bounds;
          lMap.leaflet_views.control_layers = lMap.leaflet_views.control_layers || [];

          // Clear all markers from the control layers.
          for (var groupName in lMap.leaflet_views.control_layers) {
            lMap.leaflet_views.control_layers[groupName].eachLayer(function (layer) {
              lMap.removeLayer(layer);
            });
          }

          // Add result points to their respective control layer.
          var lGroups = {};
          for (var pointIndex in points) {
            var marker = points[pointIndex];
            var lFeature = Drupal.leaflet.create_point(marker, lMap);

            var groupName = (marker.featuregroup_field) ? marker.featuregroup_field : 'default';
            if (!(groupName in lMap.leaflet_views.control_layers)) {
              lMap.leaflet_views.control_layers[groupName] = new L.LayerGroup();
              lMap.leaflet_views.control_layers[groupName].addTo(lMap);
            }
            if (marker.popup) {
              lFeature.bindPopup(marker.popup);
            }
            lMap.leaflet_views.control_layers[groupName].addLayer(lFeature);
          }

          if (lMap.leaflet_views.controls) {
            lMap.leaflet_views.controls.removeFrom(lMap);
          }
          lMap.leaflet_views.controls = L.control.layers(null, lMap.leaflet_views.control_layers);
          lMap.leaflet_views.controls.addTo(lMap);

          // Data is loaded, fire event.
          lMap.fire('dataload');
        });
      },

      openPopup: function(e) {
        var container = e.target.getContainer();
        var context = $('.leaflet-popup-content', container).get(0);
        // Attach behaviors to popup content.
        Drupal.attachBehaviors(context);
      }
    }
  };

  $(document).bind('leaflet.map', Drupal.leaflet_views.bbox.loadMap);
})(jQuery);
