<?php

/**
 * @file
 * Extension of the Views Plugin Style for Leaflet Map
 * Adapted from the GeoField Map views module and the OpenLayers Views module.
 */

class leaflet_views_plugin_style extends views_plugin_style {

  /**
   * If this view is displaying an entity, save the entity type and info.
   */
  function init(&$view, &$display, $options = NULL) {
    parent::init($view, $display, $options);

    foreach (entity_get_info() as $key => $info) {
      if ($view->base_table == $info['base table']) {
        $this->entity_type = $key;
        $this->entity_info = $info;
        break;
      }
    }
  }

  /**
   * Set default options.
   */
  function option_definition() {
    $options = parent::option_definition();
    $options['data_source'] = array('default' => '');
    $options['bbox'] = array('default' => '');
    $options['leaflet_loading'] = array('default' => 0);
    $options['name_field'] = array('default' => '');
    $options['description_field'] = array('default' => '');
    $options['featuregroup_field'] = array('default' => '');
    $options['view_mode'] = array('default' => 'full');
    $options['map'] = array('default' => '');
    $options['height'] = array('default' => '400');
    $options['height_unit'] = array('default' => 'px');
    $options['hide_empty'] = array('default' => '');
    $options['popup']['contains'] = array(
      'show' => array('default' => ''),
      'text' => array('default' => ''),
    );
    $options['zoom']['contains'] = array(
      'initialZoom' => array('default' => ''),
      'minZoom' => array('default' => 0),
      'maxZoom' => array('default' => 18),
    );
    $options['icon']['contains'] = array(
      'iconType' => array('default' => 'marker'),
      'iconUrl' => array('default' => ''),
      'shadowUrl' => array('default' => ''),
      'iconSize' => array(
        'contains' => array(
          'x' => array('default' => ''),
          'y' => array('default' => ''),
        )),
      'iconAnchor' => array(
        'contains' => array(
          'x' => array('default' => ''),
          'y' => array('default' => ''),
        )),
      'shadowAnchor' => array(
        'contains' => array(
          'x' => array('default' => ''),
          'y' => array('default' => ''),
        )),
      'popupAnchor' => array(
        'contains' => array(
          'x' => array('default' => ''),
          'y' => array('default' => ''),
        )),
      'html' => array('default' => ''),
      'iconImageStyle' => array('default' => ''),
      'htmlClass' => array('default' => ''),
    );
    $options['vector_display']['contains'] = array(
      'stroke_override' => array('default' => 0),
      'stroke' => array('default' => 1),
      'color' => array('default' => ''),
      'weight' => array('default' => ''),
      'opacity' => array('default' => ''),
      'dashArray' => array('default' => ''),
      'fill' => array('default' => 1),
      'fillColor' => array('default' => ''),
      'fillOpacity' => array('default' => ''),
      'clickable' => array('default' => 1),
    );
    return $options;
  }

  /**
   * Options form.
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    // Get list of fields in this view & flag available geodata fields:
    $handlers = $this->display->handler->get_handlers('field');

    $fields = array();
    $fields_data = array();
    foreach ($handlers as $field_id => $handler) {
      $fields[$field_id] = $handler->ui_name();

      if (!empty($handler->field_info['type']) && $handler->field_info['type'] == 'geofield') {
        $fields_data[$field_id] = $handler->ui_name();
      }
    }

    // Check whether we have a geofield we can work with:
    if (!count($fields_data)) {
      $form['error'] = array(
        '#markup' => t('Please add at least one geofield to the view'),
      );
      return;
    }

    // Map preset.
    $form['data_source'] = array(
      '#type' => 'select',
      '#title' => t('Data Source'),
      '#description' => t('Which field contains geodata?'),
      '#options' => $fields_data,
      '#default_value' => $this->options['data_source'],
      '#required' => TRUE,
    );

    // Name field.
    $form['name_field'] = array(
      '#type' => 'select',
      '#title' => t('Title Field'),
      '#description' => t('Choose the field which will appear as a title on tooltips.'),
      // '#options' => $fields,
      '#options' => array_merge(array('' => ''), $fields),
      '#default_value' => $this->options['name_field'],
    );

    $desc_options = array_merge(array(
        '' => '',
    ), $fields);

    // Add an option to render the entire entity using a view mode.
    if ($this->entity_type) {
      $desc_options += array(
        '#rendered_entity' => '<' . t('!entity entity', array('!entity' => $this->entity_type)) . '>',
      );
    }

    $form['description_field'] = array(
      '#type' => 'select',
      '#title' => t('Description Content'),
      '#description' => t('Choose the field or rendering method which will appear as a description on tooltips or popups.'),
      '#required' => FALSE,
      '#options' => $desc_options,
      '#default_value' => $this->options['description_field'],
    );

    // Grouping field.
    $form['featuregroup_field'] = array(
      '#type' => 'select',
      '#title' => t('Grouping Field'),
      '#description' => t('Select a field to group the results by. Enabling this adds a featuregroup filter to the map.'),
      '#options' => array_merge(array('' => ''), $fields),
      '#default_value' => $this->options['featuregroup_field'],
    );

    // Taken from openlayers_views_style_data::options_form().
    // Create view mode options:
    if ($this->entity_type) {

      // Get the labels (human readable) of the view modes:
      $view_mode_options = array();
      foreach ($this->entity_info['view modes'] as $key => $view_mode) {
        $view_mode_options[$key] = $view_mode['label'];
      }

      // Output the form:
      $form['view_mode'] = array(
        '#type' => 'select',
        '#title' => t('View mode'),
        '#description' => t('View modes are ways of displaying entities.'),
        '#options' => $view_mode_options,
        '#default_value' => !empty($this->options['view_mode']) ? $this->options['view_mode'] : 'full',
        '#states' => array(
          'visible' => array(
            ':input[name="style_options[description_field]"]' => array('value' => '#rendered_entity'),
          ),
        ),
      );
    }

    // Choose a map preset:
    $map_options = array();
    foreach (leaflet_map_get_info() as $key => $map) {
      $map_options[$key] = t('@label', array('@label' => $map['label']));
    }

    $form['map'] = array(
      '#title' => t('Map'),
      '#type' => 'select',
      '#options' => $map_options,
      '#default_value' => $this->options['map'] ? $this->options['map'] : '',
      '#required' => TRUE,
    );

    $form['height'] = array(
      '#title' => t('Map height'),
      '#type' => 'textfield',
      '#field_suffix' => t('px'),
      '#size' => 4,
      '#default_value' => $this->options['height'],
      '#required' => FALSE,
    );

    $form['height_unit'] = array(
      '#title' => t('Map height unit'),
      '#type' => 'select',
      '#options' => array(
        'px' => t('px'),
        '%' => t('%'),
      ),
      '#default_value' => $this->options['height_unit'] ? $this->options['height_unit'] : 'px',
      '#description' => t('Wether height is absolute (pixels) or relative (percent).'),
    );

    if (libraries_get_path('leaflet_loading')) {
      $form['leaflet_loading'] = array(
        '#title' => t('Enable Leaflet.loading'),
        '#type' => 'checkbox',
        '#description' => t("Adds a loading spinner under the map's zoom controls"),
        '#default_value' => $this->options['leaflet_loading'],
      );
    }

    $form['bbox'] = array(
      '#title' => t('Enable bounding box'),
      '#type' => 'checkbox',
      '#description' => t('Experimental: Currently visible locations will be loaded by Views GeoJSON. <i>You need to add a "Custom: bounding box" as contextual filter, option "No results found" when argument is missing.</i>'),
      '#default_value' => $this->options['bbox'],
    );

    $form['hide_empty'] = array(
      '#title' => t('Hide empty'),
      '#type' => 'checkbox',
      '#description' => t('Hide the Leaflet map if there are no results to display.'),
      '#default_value' => isset($this->options['hide_empty']) ? $this->options['hide_empty'] : TRUE,
      '#states' => array(
        'visible' => array(
          ':input[name="style_options[bbox]"]' => array('checked' => FALSE),
        ),
      ),
    );

    $form['zoom'] = leaflet_form_elements('zoom', $this->options);
    $form['icon'] = leaflet_form_elements('icon', $this->options, array('path' => 'style_options', 'fields' => $fields));
    $form['vector_display'] = leaflet_form_elements('vector_display', $this->options, array('path' => 'style_options'));
    $form['tokens'] = leaflet_form_elements('tokens', $this->options, array('weight' => 998, 'entity_type' => $this->entity_type));
  }

  /**
   * Validate the options form.
   */
  function options_validate(&$form, &$form_state) {
    if (!is_numeric($form_state['values']['style_options']['height']) || $form_state['values']['style_options']['height'] < 0) {
      form_error($form['height'], t('Map height needs to be a positive number'));
    }
  }

  /**
   * Renders view.
   */
  function render() {
    if (!empty($this->view->live_preview)) {
      return t('No preview available');
    }

    $data = array();
    $map = leaflet_map_get_info($this->options['map']);
    // Is there a geofield selected?
    if ($this->options['data_source']) {
      $this->render_fields($this->view->result);
      foreach ($this->view->result as $id => $result) {
        $geofield = $this->get_field_value($id, $this->options['data_source']);

        if (!empty($geofield)) {
          $entity = FALSE;
          // Render the entity with the selected view mode:
          if ($this->options['description_field'] === '#rendered_entity' && is_object($result)) {
            $entity = entity_load_single($this->entity_type, $result->{$this->entity_info['entity keys']['id']});
            $build = entity_view($this->entity_type, array($entity), $this->options['view_mode']);
            $description = drupal_render($build);
          }
          // Normal rendering via fields:
          elseif ($this->options['description_field']) {
            $description = $this->rendered_fields[$id][$this->options['description_field']];
          }

          $points = leaflet_process_geofield($geofield);
          
          foreach ($points as &$point) {
            // Attach pop-ups if we have rendered into $description:
            if (isset($description)) {
              $point['popup'] = $description;
            }            
            // Attach also titles & entities, they might be used later on.
            if ($this->options['name_field']) {
              $point['label'] = $this->rendered_fields[$id][$this->options['name_field']];
              if ($entity !== FALSE) {
                $point['entity'] = $entity;
              }
            }
            if ($this->options['icon']['iconType'] == 'html') {
              $target_field = $this->options['icon']['html'];
              $point['rendered_html'] = isset($this->rendered_fields[$id][$target_field]) ? $this->rendered_fields[$id][$target_field] : '';
            }
            // Add feature group if enabled.
            if (!empty($this->options['featuregroup_field'])) {
              $target_field = $this->options['featuregroup_field'];
              $point['featuregroup_field'] = isset($this->rendered_fields[$id][$target_field]) ? $this->rendered_fields[$id][$target_field] : '';
            }
          }

          // Let modules modify the points data.
          drupal_alter('leaflet_views_alter_points_data', $result, $points);
          // Merge these points into the $data array for map rendering:
          $data = array_merge($data, $points);
        }
      }
      leaflet_apply_map_settings($map, $data, $this->options, $this->entity_type);
      if (empty($data) && empty($this->options['bbox']) && !empty($this->options['hide_empty'])) {
        return '';
      }
      // Return GeoJSON when bbox argument is available.
      if (!empty($this->options['bbox']) AND $json = $this->render_geojson($data, !empty($this->view->live_preview))) {
        return $json;
      }

      $build = leaflet_build_map($map, $data, $this->options['height'] . $this->options['height_unit']);

      if (is_array($build)) {
        // Get JS settings from build array.
        // @todo: Should indices ("[0]") be considered?
        $js = &$build['#attached']['js'];
        $js_settings = &$js[0]['data']['leaflet'][0];

        // Add JS.
        $js[] = array(
          'data' => drupal_get_path('module', 'leaflet_views') . '/js/leaflet_views.js',
          'type' => 'file',
        );

        if (!empty($this->options['bbox'])) {
          // Enable bounding box in JS.
          $js_settings['map']['bbox_data_url'] = $this->view->get_url();
          $js_settings['map']['view_dom_id'] = $this->view->dom_id;

          // BBox buffer ratio, to avoid loading new points for every single move.
          // (Percent bigger than the current bbox to return results for)
          // @todo: add as setting to view.
          $js_settings['map']['buffer_ratio'] = 0.5;

          // @todo: Add default center as view setting.
          $js_settings['map']['settings'] += array(
            'center' => array(60.840279466431625, 22.66651153564453),
          );
          // Add current exposed filters.
          $js[] = array(
            'data' => array('leafletViews' => array('key' => 'value')),
            'type' => 'setting',
          );
        }

        if (!empty($this->options['leaflet_loading'])) {
          $loading = libraries_load('leaflet_loading');
          $js_settings['map']['settings']['loadingControl'] = TRUE;
        }
      }

      return $build;
    }
    return '';
  }

  /**
   * Render results as JSON for AJAX calls.
   */
  function render_geojson($data, $api_mode = FALSE) {
    $arg_pos = 0;
    // Find which argument is bounding box.
    foreach ($this->view->get_items('argument') as $arg) {
      if ($arg['field'] == 'leaflet_views_bbox_argument' && $arg['table'] == 'views' && !empty($this->view->args[$arg_pos])) {
        // Allow map definitions to provide a default icon:
        $default_icon = isset($map['icon']['iconUrl']) ? $map['icon'] : FALSE;
        foreach ($data as &$marker) {
          if (!isset($marker['icon']['iconUrl'])) {
            $marker['icon'] = $default_icon;
          }
        }
        if ($api_mode) {
          return drupal_json_encode($data);
        }
        drupal_add_http_header('Content-Type', 'application/json; charset=utf-8');
        print drupal_json_encode($data);
        drupal_exit();
      }
      ++$arg_pos;
    }
    return FALSE;
  }
}
