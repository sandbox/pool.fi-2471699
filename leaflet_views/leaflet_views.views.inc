<?php

/**
 * @file
 * Leaflet maps views integration.
 */

/**
 * Define leaflet views style.
 *
 * Implements hook_views_plugins().
 */
function leaflet_views_views_plugins() {
  $plugins = array(
    'module' => 'leaflet_views',
    'style' => array(
      'leaflet' => array(
        'title' => t('Leaflet Map'),
        'help' => t('Displays a View as a Leaflet map.'),
        'path' => drupal_get_path('module', 'leaflet_views'),
        'handler' => 'leaflet_views_plugin_style',
        'theme' => 'leaflet-map',
        'uses fields' => TRUE,
        'uses row plugin' => FALSE,
        'uses options' => TRUE,
        'uses grouping' => FALSE,
        'type' => 'normal',
        'even empty' => TRUE,
      ),
    ),
  );

  return $plugins;
}

/**
 * Adds bounding box contextual filter.
 *
 * Implements hook_views_handlers().
 */
function leaflet_views_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'leaflet_views'),
    ),
    'handlers' => array(
      'leaflet_views_bbox_argument' => array(
        'parent' => 'views_handler_argument',
      ),
    ),
  );
}

/**
 * Adds bounding box contextual filter.
 *
 * Implements hook_views_data().
 */
function leaflet_views_views_data() {
  $data = array();

  $data['views']['leaflet_views_bbox_argument'] = array(
    'group' => t('Custom'),
    'real field' => 'leaflet_views_bbox_argument',
    'title' => t('Leaflet views bounding box'),
    'help' => t('Filter locations within a bounding box.'),
    'argument' => array(
      'handler' => 'leaflet_views_bbox_argument',
    ),
  );

  return $data;
}